#!/bin/bash

#Variable
TEMPLATEHTMLFILE=/opt/iotproject/etc/index.html.template
HTMLOUTPUT=/opt/iotproject/index.html
DIRECTORY2LIST=/tmp #This is the default if no parameter is given
INTERVAL=20

if [ $# -ge 1 ]
then
    if [ -d "$1" ]
    then
        DIRECTORY2LIST=$1
    else
        echo "Directory "$1" not found." >&2 #>&2 Ausgabe der Fehlermeldung im CLI (Command-line interface)
        exit 1
    fi
fi
shift 

counter=0

while [ $counter -lt 20 ] ; do

    echo "<html><body>" > $HTMLOUTPUT
    echo "<h1>Mein Webserver</h1>" >> $HTMLOUTPUT
    date +%H:%M:%S >> $HTMLOUTPUT
    ls $DIRECTORY2LIST >> $HTMLOUTPUT
    # For Loop der die existenz zuerst prueft und dann anhängt oder fehlermeldung ausgibt
    for htmlsnippet in $*
    do 
        if [ -r "$htmlsnippet" ]
        then
            cat $htmlsnippet >> $HTMLOUTPUT
        else 
            echo "File $htmlsnippet is not readable" >&2 #>&2 Ausgabe der Fehlermeldung im CLI (Command-line interface)
        fi
    done


    echo "</body></html>" >> $HTMLOUTPUT
    sleep $INTERVAL
    counter=$(($counter+1)) # neuerdings ((counter++)) | ((xyz)) = Es wird gerechnet
done