#!/bin/bash

DIRECTORY2LIST=/tmp #This is the default 

if [ $# -eq 1 ]
then
    if [ -d "$1" ]
    then
        DIRECTORY2LIST=$1
    else
        echo "Directory "$1" not found."
        exit 1
    fi
fi

ls $DIRECTORY2LIST
