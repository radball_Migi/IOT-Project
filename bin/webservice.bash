#!/bin/bash

#Variable
TEMPLATEHTMLFILE=/opt/iotproject/etc/index.html.template
HTMLOUTPUT=/var/www/html/index.html
INTERVAL=20


mosquitto_sub -h localhost -t '#' -F "%t %p" | while read topic payload ; do
    if [ "$topic" = "sensor/temp" ] 
    then
        zeit=$(date)
        cat $TEMPLATEHTMLFILE | \
            sed "s/_ZEIT_/$zeit/g" | \
            sed "s/_TEMP_/$payload/g" > $HTMLOUTPUT
    fi
done